import logo from "./logo.svg";
import "./App.css";
import BookingTickets from "./Booking Tickets/BookingTickets";

function App() {
  return (
    <div className="App">
      <BookingTickets />
    </div>
  );
}

export default App;
