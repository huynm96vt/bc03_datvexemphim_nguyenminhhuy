let initialState = {
  danhSachGheDangDat: [],
};

export const BookingTicketReducer = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    case "CHON_GHE": {
      let danhSachGheDangDatClone = [...state.danhSachGheDangDat];
      let index = danhSachGheDangDatClone.findIndex((item) => {
        return item.soGhe == payload.soGhe;
      });
      if (index == -1) {
        danhSachGheDangDatClone.push(payload);
      } else danhSachGheDangDatClone.splice(index, 1);
      state.danhSachGheDangDat = danhSachGheDangDatClone;
      return { ...state };
    }
    case "HUY_GHE": {
      let danhSachGheDangDatClone = [...state.danhSachGheDangDat];
      let index = danhSachGheDangDatClone.findIndex((item) => {
        return item.soGhe == payload.soGhe;
      });
      if (index !== -1) {
        danhSachGheDangDatClone.splice(index, 1);
      }
      state.danhSachGheDangDat = danhSachGheDangDatClone;
      return { ...state };
    }
    default:
      return state;
  }
};
