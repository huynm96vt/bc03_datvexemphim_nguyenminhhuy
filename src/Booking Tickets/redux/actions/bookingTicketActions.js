import { CHON_GHE, HUY_GHE } from "../type/constant";

export const chonGheAction = (payload) => {
  return {
    type: CHON_GHE,
    payload,
  };
};

export const huyGheAction = (payload) => {
  return {
    type: HUY_GHE,
    payload,
  };
};
