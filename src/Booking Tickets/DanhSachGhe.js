import React, { Component, PureComponent } from "react";
import { connect } from "react-redux";
import { chonGheAction } from "./redux/actions/bookingTicketActions";

class DanhSachGhe extends PureComponent {
  renderSoGhe = () => {
    if (this.props.index == 0) {
      return this.props.hangGhe.danhSachGhe.map((ghe, index) => {
        return <button className="rowNumber">{ghe.soGhe}</button>;
      });
    }
    return this.props.hangGhe.danhSachGhe.map((ghe, index) => {
      let cssGheDaDat = "";
      let cssGheDangDat = "";
      let disabled = false;
      ghe.daDat && (disabled = true) && (cssGheDaDat = "gheDuocChon");
      let soGhe = this.props.danhSachGheDangDat.findIndex((item) => {
        return item.soGhe == ghe.soGhe;
      });

      if (soGhe !== -1) {
        cssGheDangDat = "gheDangChon";
      }
      return (
        <button
          onClick={() => {
            this.props.chonGhe(ghe);
          }}
          disabled={disabled}
          key={index}
          className={` ghe ${cssGheDangDat}  ${cssGheDaDat} `}
        >
          {ghe.soGhe}
        </button>
      );
    });
  };

  render() {
    return (
      <div>
        <div className="text-white">
          {this.props.hangGhe.hang}
          {this.renderSoGhe()}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    danhSachGheDangDat: state.BookingTicketReducer.danhSachGheDangDat,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    chonGhe: (ghe) => {
      dispatch(chonGheAction(ghe));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DanhSachGhe);
