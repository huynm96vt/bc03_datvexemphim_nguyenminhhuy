import React, { Component, PureComponent } from "react";
import { connect } from "react-redux";
import { huyGheAction } from "./redux/actions/bookingTicketActions";

class ThongTinDatGhe extends PureComponent {
  render() {
    return (
      <div>
        <div style={{ textAlign: "left" }}>
          <button style={{ marginLeft: 0 }} className="gheDuocChon"></button>
          <span className="mx-2 text-white">Ghế đã đặt</span>
          <br />
          <button style={{ marginLeft: 0 }} className="gheDangChon"></button>
          <span className="mx-2 text-white">Ghế đang chọn</span>
          <br />
          <button style={{ marginLeft: 0 }} className="ghe"></button>
          <span className="mx-2 text-white">Ghế chưa đặt</span>
        </div>

        <div className="my-3">
          <table className="table" border="3">
            <thead>
              <tr className="text-white">
                <th>Số ghế</th>
                <th>Giá</th>
                <th>Huỷ</th>
              </tr>
            </thead>
            <tbody>
              {this.props.danhSachGheDangDat.map((ghe, index) => {
                return (
                  <tr key={index} style={{ color: "#fbbf06" }}>
                    <td>{ghe.soGhe}</td>
                    <td>{ghe.gia}</td>
                    <td>
                      <span
                        onClick={() => {
                          this.props.dispatch(huyGheAction(ghe));
                        }}
                        className="text-danger"
                        style={{ cursor: "pointer", fontSize: "24px" }}
                      >
                        X
                      </span>
                    </td>
                  </tr>
                );
              })}
            </tbody>
            <tfoot>
              <tr className="text-white">
                <td>Tổng tiền</td>
                <td style={{ color: "#fbbf06" }}>
                  {this.props.danhSachGheDangDat.reduce(
                    (tongTien, gheDangDat, index) => {
                      return (tongTien += gheDangDat.gia);
                    },
                    0
                  )}
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    danhSachGheDangDat: state.BookingTicketReducer.danhSachGheDangDat,
  };
};

export default connect(mapStateToProps)(ThongTinDatGhe);
