import React, { Component } from "react";
import "./BookingTickets.css";
import ThongTinDatGhe from "./ThongTinDatGhe";
import dataDanhSachGhe from "../data/danhSachGhe.json";
import DanhSachGhe from "./DanhSachGhe";

export default class BookingTickets extends Component {
  renderHangGhe = () => {
    return dataDanhSachGhe.map((hangGhe, index) => {
      return (
        <div key={index}>
          <DanhSachGhe index={index} hangGhe={hangGhe} />
        </div>
      );
    });
  };
  render() {
    return (
      <div
        className="bookingTicket"
        style={{
          position: "fixed",
          width: "100%",
          height: "100%",
          background: "url('./img/bgmovie.jpg') no-repeat ",
          backgroundSize: "cover",
        }}
      >
        <div
          style={{
            position: "fixed",
            width: "100%",
            minHeight: "100%",
            background: "rgba(0,0,0,0.7)",
          }}
        >
          <div className="container-fluid">
            <div className="row">
              <div className="col-8">
                <h1 className=" py-2 text-warning">ĐẶT VÉ XEM PHIM</h1>
                <div
                  className="mt-5"
                  style={{ display: "flex", justifyContent: "center" }}
                >
                  <div className="screen"></div>
                </div>
                {this.renderHangGhe()}
              </div>
              <div className="col-4">
                <h1 className="py-2  text-white">DANH SÁCH GHẾ BẠN CHỌN </h1>
                <ThongTinDatGhe />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
